import {Component} from 'angular2/core';
import {Title} from 'angular2/platform/browser';
import {MATERIAL_DIRECTIVES} from 'ng2-material/all';

@Component({
	selector: 'home-component',
	template: require('./home.html'),
	styles: [require('./home.scss')],
	providers: [Title],
	directives: [MATERIAL_DIRECTIVES]
})
export class HomeComponent {
	constructor(public title: Title) {
		this.title.setTitle("Home page");
	}
}