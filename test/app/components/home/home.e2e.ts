describe('HomeComponent', function () {
	beforeEach(function () {
		browser.get('/home');
	});

	it('should have a title', function () {
		var subject = browser.getTitle();
		var result = 'Home page';
		expect(subject).toEqual(result);
	});
});