/**
 * Lets retrieve all the files that end with .spec.ts within this current directory
 * This file is loaded by the karma configuration which in turn loads all the required files.
 */
require('./app/test.main.ts');
var testContext = require.context('.', true, /\.spec\.ts/);
testContext.keys().forEach(testContext);